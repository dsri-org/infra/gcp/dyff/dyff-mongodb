# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "credentials" {
  value     = module.root.credentials
  sensitive = true
}

output "endpoints" {
  value = module.root.endpoints
}

output "hostname" {
  value = module.root.hostname
}
