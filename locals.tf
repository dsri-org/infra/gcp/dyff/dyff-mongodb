# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  cluster_name = "${var.environment}-dyff-cloud"
  deployment   = "dyff-mongodb"
  name         = "${var.environment}-${local.deployment}"
  region       = "us-central1"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
    manager     = "terraform"
  }

  replica_count = 3

  replica_set_name = "mongodb"

  endpoints = [
    for index in range(local.replica_count) : "${local.replica_set_name}-${index}.mongodb-headless.mongodb.svc.cluster.local:27017"
  ]

  mongodb_hostname_string = join(",", local.endpoints)

  mongodb_users = [
    {
      database = "accounts"
      username = "auth"
      role     = "readWrite"
    },
    {
      database = "workflows"
      username = "command"
      role     = "readWrite"
    },
    {
      database = "workflows"
      username = "query"
      role     = "read"
    }
  ]

  mongodb_users_map = {
    for user in local.mongodb_users : user.username => user
  }

  mongodb_database_credentials_map = {
    for user, data in local.mongodb_users_map : user => merge(data, {
      password   = random_password.mongodb_passwords[user].result
      credential = "${data.username}:${random_password.mongodb_passwords[user].result}"
    })
  }
  mongodb_init_scripts = {
    for user in local.mongodb_users : "grant-${user.username}-${user.database}.js" => templatefile(
      "${path.module}/scripts/mongodb/grant-user.js.tpl",
      { user = user }
    )
  }

  credentials_pre_map = {
    for user, data in local.mongodb_database_credentials_map : user => merge(data, {
      connection_string = "mongodb://${data.username}:${random_password.mongodb_passwords[user].result}@${local.mongodb_hostname_string}/${data.database}?ssl=false&replicaSet=${local.replica_set_name}"
    })
  }

  credentials_map = {
    for user, data in local.credentials_pre_map : user => merge(data, {
      connection_string_escaped = replace(data.connection_string, "/[,.=]/", "\\$0")
    })
  }
}
