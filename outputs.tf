# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "credentials" {
  value     = local.credentials_map
  sensitive = true
}

output "endpoints" {
  value = local.endpoints
}

output "hostname" {
  value = local.mongodb_hostname_string
}
