# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "mongodb" {
  metadata {
    name = "mongodb"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

resource "random_password" "mongodb_root_user" {
  length  = 32
  special = false
}

resource "random_password" "mongodb_root_password" {
  length  = 64
  special = false
}

resource "random_password" "mongodb_passwords" {
  for_each = local.mongodb_users_map

  length  = 32
  special = false
}

resource "random_password" "replica_set_key" {
  length  = 64
  special = false
}

resource "kubernetes_secret" "mongodb" {
  metadata {
    name      = "mongodb-passwords"
    namespace = kubernetes_namespace.mongodb.metadata[0].name
  }

  data = {
    mongodb-passwords       = join(",", [for user in local.mongodb_users : random_password.mongodb_passwords[user.username].result])
    mongodb-replica-set-key = random_password.replica_set_key.result
    mongodb-root-password   = random_password.mongodb_root_password.result
    mongodb-root-user       = random_password.mongodb_root_user.result
  }
}

# https://artifacthub.io/packages/helm/bitnami/mongodb
resource "helm_release" "mongodb" {
  name       = "mongodb"
  namespace  = kubernetes_namespace.mongodb.metadata[0].name
  repository = "oci://registry-1.docker.io/bitnamicharts"
  version    = "15.0.2"
  chart      = "mongodb"

  skip_crds = true

  wait          = false
  wait_for_jobs = false

  values = [yamlencode({
    arbiter = {
      enabled = false
    }

    architecture = "replicaset"

    auth = {
      databases      = [for user in local.mongodb_users : user.database]
      usernames      = [for user in local.mongodb_users : user.username]
      existingSecret = kubernetes_secret.mongodb.metadata[0].name
    }

    initdbScripts = local.mongodb_init_scripts

    metrics = {
      enabled = true
    }

    networkPolicy = {
      enabled = true
    }

    persistentVolumeClaimRetentionPolicy = {
      enabled     = true
      whenScaled  = "Retain"
      whenDeleted = "Retain"
    }

    podAntiAffinityPreset = "hard"

    replicaCount        = local.replica_count
    replicaSetHostnames = true
    replicaSetName      = local.replica_set_name

    resourcesPreset = "large"

    topologyKey = "topology.kubernetes.io/zone"
  })]

  set_sensitive {
    name  = "auth.rootUser"
    value = random_password.mongodb_root_user.result
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest
# https://cloud.google.com/stackdriver/docs/managed-prometheus/setup-managed#gmp-servicemonitor
resource "kubernetes_manifest" "pod_monitoring" {
  manifest = {
    apiVersion = "monitoring.googleapis.com/v1"
    kind       = "PodMonitoring"
    metadata = {
      name      = "mongodb-metrics"
      namespace = kubernetes_namespace.mongodb.metadata[0].name
    }
    spec = {
      selector = {
        matchLabels = {
          "app.kubernetes.io/instance"  = "mongodb"
          "app.kubernetes.io/name"      = "mongodb"
          "app.kubernetes.io/component" = "mongodb"
        }
      }
      endpoints = [{
        port = "metrics"
        path = "/metrics"
      }],
    }
  }
}
