#!/bin/bash
set -euo pipefail

kubectl port-forward -n mongodb sts/mongodb mongodb
